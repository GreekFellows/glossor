#include <iostream>
#include <string>
#include <sstream>
#include <fstream>
#include <deque>
#include <cctype>

void get_words(
	std::deque<std::string>	&words_deque,
	const std::string		&str
);

void get_glosses(
	std::deque<std::string>	&glosses_deque,
	const std::string		&str
);

void get_trans(
	std::string				&trans_str,
	const std::string		&str
);

void write_html(
	std::deque<std::string>	&words,
	std::deque<std::string>	&glosses,
	const std::string		&trans,
	std::ostream			&writer
);

void get_words(
	std::deque<std::string> &words_deque,
	const std::string &str
) {
	std::string word;
	std::stringstream ss(str);
	while (ss >> word) {
		words_deque.push_back(word);
	}
}

void get_glosses(
	std::deque<std::string> &glosses_deque,
	const std::string &str
) {
	std::string gloss;
	std::stringstream ss(str);
	while (ss >> gloss) {
		glosses_deque.push_back(gloss);
	}
}

void get_trans(
	std::string &trans_str,
	const std::string &str
) {
	std::string::size_type first_nonws = std::string::npos, last_nonws = std::string::npos;

	for (std::string::size_type i = 0, s = str.size(); i < s; ++i) {
		char c = str[i];
		if (!std::isspace(c)) {
			if (first_nonws == std::string::npos) {
				first_nonws = i;
			}

			last_nonws = i;
		}
	}

	trans_str = str.substr(first_nonws, last_nonws + 1);
}

void write_html(
	std::deque<std::string>	&words,
	std::deque<std::string>	&glosses,
	const std::string		&trans,
	std::ostream			&writer
) {
	static const std::string
		&gloss_header =
			"<div class=\"gloss\" style=\"display: inline-grid;\">\n"
			"\t<div class=\"gloss-cols\" style=\"display: flex; flex-direction: row; flex-wrap: wrap;\">\n",
		&gloss_col_header =
			"\t\t<div class=\"gloss-col\" style=\"padding: 10px 10px 0px 0px;\">\n"
			"\t\t\t<span class=\"gloss-col-word\" style=\"font-weight: bold; font-style: italic;\">",
		&gloss_col_inter =
			"</span>\n"
			"\t\t\t<br />\n"
			"\t\t\t<span class=\"gloss-col-gloss\">",
		&gloss_col_footer =
			"</span>\n"
			"\t\t</div>\n",
		&gloss_trans_header =
			"\t</div>\n"
			"\t<div class=\"gloss-trans\" style=\"padding: 10px 10px 0px 0px;\">\n"
			"\t\t<span>",
		&gloss_trans_footer =
			"</span>\n"
			"\t</div>\n",
		&gloss_footer =
			"</div>";

	writer << gloss_header;

	while (!words.empty()) {
		std::string word = words.front();
		std::string gloss = glosses.front();

		writer << gloss_col_header << word << gloss_col_inter << gloss << gloss_col_footer;

		words.pop_front();
		glosses.pop_front();
	}

	writer << gloss_trans_header << trans << gloss_trans_footer << gloss_footer;
}

int main(int argc, char **argv) {
	if (argc < 2) {
		std::cout << "usage: glossor input.file [output.file]\n";
		return 0;
	}

	std::string input_path = argv[1];
	std::string output_path;
	if (argc >= 3) {
		output_path = argv[2];
	}
	else {
		std::string::size_type dot_index = input_path.find_last_of('.');
		if (dot_index == std::string::npos) {
			dot_index = input_path.size();
		}
		output_path = input_path.substr(0, dot_index) + ".out" + input_path.substr(dot_index);
	}

	std::ifstream reader(input_path);
	std::ofstream writer(output_path);

	std::stringstream comment_strm;

	bool glossing = false;
	size_t gloss_line = 0;
	std::deque<std::string> words, glosses;
	std::string trans;

	std::string line;
	while (std::getline(reader, line)) {
		if (!glossing) {
			std::string::size_type index = line.find("gloss {");
			if (index != std::string::npos) {
				writer << line.substr(0, index);

				glossing = true;
			}
			else {
				writer << line << std::endl;
			}
		}
		else {
			std::string::size_type index = line.find("}");
			if (index != std::string::npos) {
				std::string tmp;

				writer << "<!--glossed {" << std::endl;
				while (std::getline(comment_strm, tmp)) {
					writer << tmp << std::endl;
				}
				writer << "}-->";
				write_html(words, glosses, trans, writer);
				writer << line.substr(index + 1) << std::endl;

				glossing = false;
			}
			else {
				comment_strm << line << std::endl;

				if (gloss_line == 0) {
					get_words(words, line);
				}
				else if (gloss_line == 1) {
					get_glosses(glosses, line);
				}
				else if (gloss_line == 2) {
					get_trans(trans, line);
				}
				++gloss_line;
			}
		}
	}
}
