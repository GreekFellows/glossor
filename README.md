# glossor

a HTML gloss generator using flex-box

## how to build

This project uses CMake. Use CMake to generate a makefile for your favorite `make`, then `make` to actually build the program.

1.	`cd` into `build`
1.	`cmake ..` You may want to specify a Makefile generator by `-G <generator-name>`. Run `cmake -h` to see a list of supported generators.
1.	`make` using the generated makefile.

## how to use

```
glossor <input-file-path> [<output-file-path>]
```

where `<input-file-path>` (mandatory) is the path of the file which contains the gloss, and `<output-file-path>` (optional) is the path of the generated output.

If `<output-file-path>` is not specified, the output path is `<pre-extension>.out<extension>` if the input path contains an extension, and `<input-file-path>.out` otherwise.

## gloss syntax

Strictly follow the following format.

```
gloss {
	word0   word1   word2   word3   ...
	gloss0  gloss1  gloss2  gloss3  ...
	translation
}
```

Words and glosses must be separated by contiguous whitespace characters (preferrably tabs or spaces).

Here is an example from `dummy`:

```
gloss {
	Taro-ga     Hanako-wo   ket-ta
	Taro-NOM    Hanako-ACC  kick-PAST
	'Taro kicked Hanako.'
}
```

## license

By [@GreekFellows](https://gitlab.com/GreekFellows). This project is under the MIT license.
